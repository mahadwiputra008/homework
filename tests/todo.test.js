const app = require("../"); // Ganti dengan path yang sesuai ke berkas aplikasi Anda
const request = require('supertest');

describe('Todo Unit Test', () => {
  test('Get Message Success', (done) => {
    request(app)
      .get('/todos')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.success).toBe('success')
        done()
      })
      .catch(done)
  })
})
