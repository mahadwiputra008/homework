const express = require("express");
const todoRoute = require("./routes/todos");
const logger = require("./middleware/logger");
const errorHandler = require("./middleware/errorHandler");
const bodyParser = require("body-parser");
require('dotenv').config()

const app = express();
const port = process.env.PORT;
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger);

app.use('/', todoRoute);
app.get('/', (req, res) => {
  res.send('Hello World 5');
});
app.use(errorHandler);

// connectDB();
app.listen(port, () => console.log(`Server Running On Port: ${port}`));

module.exports = app;