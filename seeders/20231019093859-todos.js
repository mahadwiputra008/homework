'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Todos', [
      {
        taskName: 'Belajar',
        description: 'Belajar Programming Javascript',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        taskName: 'Bermain',
        description: 'Bermain Bersama Anak',
        createdAt: new Date(),
        updatedAt: new Date()
      },
  ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Todos', null, {});
  }
};
