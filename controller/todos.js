const { Todo } = require("../models");
const {Op} = require("sequelize");

class TodosController {
  async getAllTodo(req, res, next) {
    try {
      const result = await Todo.findAll();
      res.status(200).json({
        "success": "success",
        "data": result
      })
    } catch (error) {
      next(error);
    }
  }

  async getTodoById(req, res, next) {
    try {
      const id = req.params.id;
      const result = await Todo.findAll({
        where: {
          id: id,
          deletedAt: {
            [Op.is]: null
          }
        }
      });
      if(res.status(200)){
        if(!result.length){
          res.send('Data Not Found');
        }else {
          res.json({
            "success": true,
            "message": result
          })
        }
      }
    } catch (error) {
      next(error);
    }
  }

  async addTodo(req, res, next) {
    try {
      const {taskName, description} = req.body;
      const result = await Todo.create({taskName, description});
      res.status(200).json({
        "success": true,
        "message": console.log(`Success Insert Data`)
      })
    } catch (error) {
      next(error);
    }
  }

  async deleteTodo(req, res, next) {
    try {
      const id = req.params.id;
      const result = await Todo.destroy({
        where: {
          id: id
        }
      });
      res.status(200).json({
        "success": true,
        "message": console.log(`Success Delete Data`)
      })
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new TodosController();