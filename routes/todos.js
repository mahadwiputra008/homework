const express = require("express");
const TodosController = require("../controller/todos");
const route = express.Router();

route.get('/todos', TodosController.getAllTodo);
route.get('/todos/:id', TodosController.getTodoById);
route.post('/todos', TodosController.addTodo);
route.delete('/todos/:id', TodosController.deleteTodo);

module.exports = route;